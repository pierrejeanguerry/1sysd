#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int palindrome(char *str){
/*
	int length;
	int count = 0;
	length = strlen(str) - 1;
	while (count <= length / 2){
		if (str[count] != str[length - count])
			return 0;
		count++;
	}
	return 1;
*/
	char *buff = str;
	char *str2;
	int length = 0;
	int count = 0;

	while(*buff++)
		length++;
	str2 = buff - 2;
	while (*str && *str == *str2 && length > count * 2){
		str++;
		str2--;
	//	printf("%d\n", count);
	//	printf("%d\n\n", length);
		count++;
	}
	return length < count * 2;
}

void usage(char *prog) {
        printf("Usage: %s word"
                "\n\n  Tell if word is a palindrome"
                "\n  0 -> No, 1 -> Yes\n", prog);
}

int main(int argc, char *argv[]){
	printf("%d\n",palindrome("abba"));
	printf("%d\n",palindrome("radar"));
	printf("%d\n",palindrome("raydar"));
}
