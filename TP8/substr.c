#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int substr(char *str1, char *str2){
	int pos = 0;
	char *buff1;
	char *buff2;

	if (strlen(str2) == 0)
		return 0;
	while (*str1){
		buff1 = str1;
		buff2 = str2;
		while (*buff1 && *buff2 && *buff1 == *buff2){
			buff1++;
			buff2++;
		}
		if (*buff2 == '\0')
			return pos;
		pos++;
		str1++;
	}
	return -1;
}

int main(int argc, char **argv){
	int ret;

        ret = substr(argv[1],argv[2]);
        printf("%d\n", ret);



/*
	printf("%d\n", substr("to be", "to"));
	printf("%d\n", substr("to be", "be"));
	printf("%d\n", substr("t", "t"));
	printf("%d\n", substr("b", "be"));
	printf("%d\n", substr("", ""));
	printf("%d\n", substr("", " "));
*/

}
