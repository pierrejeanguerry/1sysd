#include <stdio.h>

int main(){
	char tab[] = "je mange des pattes au beurre";
	char decomp[50][20];
	char *pdebut;
	char *pfin;
	int nbmot = 0;
	int cursor;

	pdebut = tab;
	pfin = tab;

	while (*pfin){
		if (*pfin == ' '){
			cursor = 0;
			while (pdebut != pfin){
				decomp[nbmot][cursor++] = *pdebut++;
			}
			decomp[nbmot][cursor] = '\0';
			nbmot++;
			while (*pdebut == ' ' ){
				*pdebut++;
			}
			pfin = pdebut;
		}
		else{
			*pfin++;
		}
	}
	if (pdebut != pfin){
		cursor = 0;
		while (pdebut != pfin){
			decomp[nbmot][cursor++] = *pdebut++;
		}
		decomp[nbmot][cursor] = '\0';
		nbmot++;
	}

	cursor = 0;
	for (int count = 0; count < nbmot; count++){
		pdebut = decomp[count];
		while (*pdebut != '\0'){
			printf("%c", *pdebut++);
		}
		printf("\n");
	}

	return 0;
}
