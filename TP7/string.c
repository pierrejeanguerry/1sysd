#include <stdio.h>
#include <string.h>

int main(){
	char str[] = "abcdefghijklmnopqrstuvwxyz";
	char *p;

	for(int count = 0; count < strlen(str); count++){
		printf("%c", str[count]);
	}
	printf("\n");
	p = &str[0];
	while(*p != '\0'){
		printf("%c", *p++);
	}
	printf("\n");
	return 0;
}
