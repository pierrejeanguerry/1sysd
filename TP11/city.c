#include<stdio.h>
#include<stdlib.h>
#include<string.h>
// exo_struct
// partie 1
// compléter les fonctions et ajouter du code
// de test dans main()
// https://framagit.org/jpython/1SYSD/ exo_struct
// partie 2 : créer un tableau de villes (avec deux ou trois villes)
// et affichez les infos sur toute les villes dans une boucle
// for.
typedef struct City City;
struct City {
    char name[50];
    int  pop;
    long double area;
};

City ask_city() {
    City city;

    printf("Nom : ");
    scanf("%s", city.name);
    printf("Population : ");
    scanf("%d", &city.pop);
    printf("Surface : "); 
    scanf("%Lf", &city.area);
    
    return city;
}

void print_city(City city) {
   printf("%s: %d hab. %.2Lfkm2\n", city.name, city.pop, city.area);
}

long double density(City city) {
   return city.pop / city.area;
}

int main() {
    City paris = { "Paris", 2175601, 105.4 };
    City londres = { "London", 8908081, 1572.0 };
    City newcity;
    City cities[4];
    City *c;

    //printf("%s, %d hab. %.2Lfkm2\n", paris.name, paris.pop, paris.area);
    //printf("%s, %d hab. %.2Lfkm2\n", londres.name, londres.pop, londres.area);
    print_city(paris);
    printf("Densité : %.2Lf ha/km2\n", density(paris));
    print_city(londres);
    newcity = ask_city();
    print_city(newcity);

    cities[0] = paris;
    cities[1] = londres;
    cities[2] = newcity;

    c = malloc(sizeof(City));
    //c->name = "Donaldville";
    strcpy(c->name, "Donaldville");
    c->pop  = 10;
    c->area = 123.1; 

    cities[3] = *c;

    for (int i=0; i < 4; i++) {
        print_city(cities[i]);
    }

}
