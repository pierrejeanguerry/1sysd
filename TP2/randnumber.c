#include<stdio.h>
#include<stdlib.h>

int main(){
	int number, user_number;
	number = rand() % 10 + 1;
	printf("Donner votre chiffre entre 1 et 10: ");
	scanf("%d", &user_number);
	if (user_number < number){
		printf("Plus grand\n");
	} else if (user_number > number){
		printf("Plus petit\n");
	} else {
		printf("Trouve !\n");
	}
	printf("%d\n", number);
	return 0;
}
