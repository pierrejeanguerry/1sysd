#include<stdio.h>




int main(){
/*	float mat1[3][3] = {{1.0, 2.0, 3.0},
			  {4.0, 5.0, 6.0},
			  {7.0, 8.0, 9.0}};

	float mat2[3][3] = {{9.0, 8.0, 7.0},
			  {6.0, 5.0, 4.0},
			  {3.0, 2.0, 1.0}};
       	
			  */
	float mat1[3][3];
	float mat2[3][3];
	float prod[3][3];

	printf("Premiere Matrice:\n");
	scanf("%f %f %f", &mat1[0][0], &mat1[0][1], &mat1[0][2]);
	scanf("%f %f %f", &mat1[1][0], &mat1[1][1], &mat1[1][2]);
	scanf("%f %f %f", &mat1[2][0], &mat1[2][1], &mat1[2][2]);
	
	printf("Deuxieme Matrice:\n");
	scanf("%f %f %f", &mat2[0][0], &mat2[0][1], &mat2[0][2]);
	scanf("%f %f %f", &mat2[1][0], &mat2[1][1], &mat2[1][2]);
	scanf("%f %f %f", &mat2[2][0], &mat2[2][1], &mat2[2][2]);
	for(int prod_line = 0; prod_line < 3; prod_line++){
		for(int prod_col = 0; prod_col < 3; prod_col++){
			for(int cursor = 0; cursor < 3; cursor++){
				prod[prod_line][prod_col] += mat1[prod_line][cursor] *
							     mat2[cursor][prod_col];
			}
		}
	}
	
	printf("Resultat produit :\n");

	for(int prod_line = 0; prod_line < 3; prod_line++){
		for(int prod_col = 0; prod_col < 3; prod_col++){
			printf("%.2f ",prod[prod_line][prod_col]);
		}
		printf("\n");
	}
	return 0;
}
