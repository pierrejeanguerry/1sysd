#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

typedef struct node node;
struct node {
   int val;
   node *next;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    return p;
}

void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}

int length_list(node *head){
	node *walk;
	int count = 0;

    	walk = head;
   	while (walk != NULL) {
        	count++;
		walk = walk->next;
    	}
	return count;
}

node *append_val(node *head, int val) {
	node *walk;
    	
	if (!head){
		head = create_node(val);
		return head;
	}
	else {
		walk = head;
   	while (walk->next != NULL) {
			walk = walk->next;
   	}
		walk->next = create_node(val);
	}
	return head;
}

void *append_val2(node **phead, int val) {
	node **pwalk;	
   
	pwalk = phead;
	while (*pwalk != NULL) {
		pwalk = &((*pwalk)->next);
    }
	*pwalk = create_node(val);
}



int search_val(node *head, int val){
	int pos = 0;
	node *walk;

	walk = head;
	while (walk && walk->val != val){
		walk = walk->next;
		pos++;
	}
	if (!walk)
		return -1;
	return pos;

}

node *search_node(node *head, int val){
	int pos = 0;
	node *walk;

	walk = head;
	while (walk && walk->val != val){
		walk = walk->next;
		pos++;
	}
	if (!walk)
		return NULL;
	return walk;

}

node *suppr_node1(node *head, node *elem){
	node *walk;
	node *prev = NULL;

	walk = head;
	while (walk != elem && walk){
		prev = walk;
		walk = walk->next;
	}
	if (!prev){
		head = elem->next;
	}
	else {
		prev->next = elem->next;
	}
	if (elem){
		free(elem);
	}
	return head;
}

void suppr_node2(node **phead, node *elem){
	node **walk;

	walk = phead;
	while (*walk != elem && *walk){
		walk = &((*walk)->next);
	}
	*walk = elem->next;
	if (elem){
		free(elem);
	}
}

void free_list(node **phead){
	node *next = NULL;
	while(*phead){
		next = (*phead)->next;
		free(*phead);
		*phead = next;
		//print_list(*phead);
	}
	*phead = NULL;
}

/*
void reverse_list(node **phead){
	node *new_head = NULL;
	node **walk;

	walk = phead;
	while (*walk){
		insert_val(&new_head, (*walk)->val);
		walk = &((*walk)->next);
	}
	free_list(phead);
	*phead = new_head;
}
*/

void insert_val(node **phead, int val){

	node *oldhead;

	oldhead = *phead;
	*phead = create_node(val);
	(*phead)->next = oldhead;

}

void insert_entry(node **phead, node **entry){

	(*entry)->next = *phead;
	*phead = *entry;
}


void reverse_list(node **phead){
	node *inverse = NULL;
	node *buffer = NULL;
	
	while (*phead){
		buffer = *phead;
		phead = &((*phead)->next);
		buffer->next = inverse;
		inverse = buffer;
		print_list(inverse);
	}
	*phead = inverse;
}

int main() {
    node *head = NULL, *current;
    node *head1 = NULL, *head2 = NULL;

 
	current = create_node(15);
	insert_entry(&head1, &current);
    print_list(head1);
	
	insert_val(&head1, 50);
    head1 = append_val(head1, 42);
    head1 = append_val(head1, 12);
    head1 = append_val(head1, -5);
    head1 = append_val(head1, 41);
    print_list(head1);
/*	
	current = create_node(30);
	insert_entry(&head1, current);
	current = create_node(1);
	insert_entry(&head1, current);
	current = create_node(18);
	insert_entry(&head1, current);
    print_list(head1);
*/


	reverse_list(&head1);
//    print_list(head1);

	free_list(&head1);
}
