# Listes simplement chaînées

## Partant de linkedlist_start.c

- Ajouter une fonction qui calcule la longeur d'une liste chaînées 
- Ajouter une fonction qui insère une valeur en fin de liste
  - La fonction reçoit un pointeur vers le premier nœud
  - Elle renvoie un donnée de même type, pourquoi c'est nécessaire ?
- On peut envisager une variante de cette fonction qui reçoit l'adresse
  d'un pointeur vers le premier nœud
  - Qu'est-ce que ça permet de plus ?
  - Le code de la fonction est-il plus simple ?

