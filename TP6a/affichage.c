void afficher_matrice(long double matrice[3][3]) {
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            printf("%2.2Lf ", matrice[i][j]);
        }
        printf("\n");
    }
}
