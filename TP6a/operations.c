void multiplier_matrice(long double mat1[3][3], long double mat2[3][3], \
                long double resultat[3][3]) {
     int i,j,k;
     for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
           resultat[i][j] = 0;
           for (k = 0; k < 3; k++) {
                   resultat[i][j] += mat1[i][k] * mat2[k][j];
           }
        }
     }
}
