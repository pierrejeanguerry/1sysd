void saisir_matrice(long double matrice[3][3]) {
    int i, j;
    printf("Saisir les éléments de la matrice :\n");
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            scanf("%Lf", &matrice[i][j]);
        }
    }
}

