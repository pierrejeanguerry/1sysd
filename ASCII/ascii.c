#include <stdio.h>

int main(){

	for(int i = 32; i <= 126; i++){
		if ((i - 31) % 72 == 0){
			printf("\n");
		}
		printf("%c", i);
	}
	printf("\n");
	return 0;
}
