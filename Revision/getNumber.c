#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int isNumber(char c){
	if (c >= '0' && c <= '9')
		return 1;
	return 0;
}

int count_str(char *str){
	int strnb = 0;
	int count = 1;

	if (isNumber(str[0]) == 1)
		strnb++;
	while(str[count]){
		if (isNumber(str[count]) == 1 && isNumber(str[count - 1]) == 0){
			strnb++;
		}
		count++;
	}
	return strnb;
}

int nbLength(char *str){
	int length = 0;

	while(*str && isNumber(*str) == 1){
		length++;
		str++;
	}
	return length;
}

/*char **getNumber(char *str){
	int strnb;
	int length;
	char **tab;
	int wordn = 0;
	int cursor = 0;

	strnb = count_str(str);
	tab = malloc(sizeof(char*) * (strnb + 1));
	while (*str){
		if (isNumber(*str) == 1){
			cursor = 0;
			length = nbLength(str);
			tab[wordn] = malloc(sizeof(char) * (length + 1));
			while (isNumber(*str) == 1){
				tab[wordn][cursor] = *str;
				str++;
				cursor++;
			}
			tab[wordn][cursor] = '\0';
			wordn++;
		}
		else {
			str++;
		}
	}
	tab[wordn] = malloc(sizeof(char));
	tab[wordn][0] = '\0';
	return tab;
}
*/


long int *getNumber(char *str){
	int strnb;
	long int *tab;
	int wordn = 0;

	strnb = count_str(str);
	tab = malloc(sizeof(long int) * (strnb + 1));
	while (*str){
		if (isNumber(*str) == 1){
			while (isNumber(*str) == 1){
				tab[wordn] *= 10;
				tab[wordn] += *str - 48;
				str++;
			}
			wordn++;
		}
		else {
			str++;
		}
	}
	tab[wordn] = -1;
	return tab;
}

int main(int argc, char *argv[]){
	
	int count = 0;
/*
	char **tab;
	tab = getNumber(argv[1]);
	while (tab[count][0] != '\0'){
		printf("%s ", tab[count]);
		count++;
	}
	printf("\n");
	count = 0;
	while (tab[count][0] != '\0'){
		free(tab[count]);
		count++;
	}
	free(tab[count]);
	free(tab);
*/
	long int *tab;
	tab = getNumber(argv[1]);
	while (tab[count] != -1){
		printf("%ld ",tab[count]);
		count++;
	}
	printf("\n");
	free(tab);
	return 0;
}
