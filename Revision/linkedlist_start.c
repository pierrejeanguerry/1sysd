#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>

typedef struct node node;
struct node {
   int val;
   node *next;
   node *prev;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    p->prev = NULL;
    return p;
}
/*
void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}
*/

void print_list(node *head, char c) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
		if (c == 'n')
        	walk = walk->next;
		if (c == 'p')
        	walk = walk->prev;
    }
    printf("\n");
}

//node *append_val(node *head, int val) {

void *append_val2(node **phead, int val) {
	node **pwalk;
	node **pprev = NULL;
	pwalk = phead;
	while ((*pwalk)){
		pprev = pwalk;
		pwalk = &((*pwalk)->next);
	}
	*pwalk = create_node(val);
	//(*pwalk)->prev = *(pprev); ne fonctionne pas jsais pas pk
}

void insert_val(node **phead, int val){

    node *oldhead;

    oldhead = *phead;
    *phead = create_node(val);
    (*phead)->next = oldhead;
	(*phead)->prev = oldhead->prev;
	oldhead->prev = *phead;

}

void suppr_node2(node **phead, node *elem){
    node **walk;

    walk = phead;
    while (*walk != elem && *walk){
        walk = &((*walk)->next);
    }
    *walk = elem->next;
    if (elem){
        free(elem);
    }
}


int main() {
   
    node *head = NULL;
    
	append_val2(&head, 42);
    append_val2(&head, 12);
    append_val2(&head, -5);
    append_val2(&head, 41);

    print_list(head, 'n');
	while (head->next){
		head = head->next;
	}
    print_list(head, 'p');

}
