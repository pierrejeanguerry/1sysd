#include <stdio.h>
#include <stdlib.h>

int isPrimeNumber(int nb){
	if (nb == 0)
		return 0;
	for (int count = 2; count < nb/2; count++){
		for (int count2 = 2; count2 < nb; count2++){
			if (count * count2 == nb)
				return 0;
			}
	}
	return 1;
}

int *sieve(int n){
	int *tab;
	int cursor = 1;

	tab = malloc(sizeof(int) * n);
	if (!tab)
		exit(EXIT_FAILURE);
	tab[0] = 2;
	for (int count = 3; count < n; count = count + 2){
		tab[cursor] = count;
		if (isPrimeNumber(count) == 0)
			tab[cursor] = 0;
		cursor++;
	}
	return tab;
}

int main(){
	int *tab;
	int nb = 100;

	tab = sieve(nb);
	for (int count = 0; count < nb; count++){
		if (tab[count] != 0)
			printf("%d ", tab[count]);
	}
	free(tab);
	printf("\n");
}
