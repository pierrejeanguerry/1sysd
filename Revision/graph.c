#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#define WIDTH 80
#define HEIGHT 24

int the_graph[WIDTH][HEIGHT];

float f(float x){
	return 2;
}

void init_graph(){
	for (int height = 0; height < HEIGHT; height++){
		for (int width = 0; width < WIDTH; width++){
			the_graph[width][height] = 0;
		}
	}
}

int giveY(float y1, float y2, float y_length, float result){
	int count = 0;
	printf("\n");
	for (float buffer = y2; buffer > y1; buffer -= y_length / HEIGHT){
		if (buffer < result)
			return count--;
		if (buffer == result)
			return count;
		count++;
	}
	return -1;
}


void put_axis(float x1, float x2, float y1, float y2){
	int x = 0;
	int y = 0;
	float count;
	int cursor = 0;
	
	count = x1;
	while (count < 0 && count <= x2){
		printf("count = %f\n", count);
		count += (x2 -x1)/WIDTH;
		x++;
	}
	
	if (count >= 0 && count <= x2){
		x--;
		for (cursor = 0; cursor < HEIGHT; cursor++){
			the_graph[x][cursor] = 2;
		}
	}
	for (count = y2; count > 0 && count >= y1; count -= (y2 - y1) / HEIGHT){
		y++;
	}
	
	if (count <= 0 && count >= y1){
		for (cursor = 0; cursor < WIDTH; cursor++){
			the_graph[cursor][y] = 3;
		}
	}
}


void graph(float x1, float x2, float y1, float y2){
	float x_length = x2 - x1;
	float y_length = y2 - y1;
	float result;
	int y, x = 0;

	init_graph();
	for (float count = x1; count <= x2; count += x_length / WIDTH){
		
		result = f(count);
		if (result >= y1 && result <= y2){
			y = giveY(y1,y2, y_length,result);
			if (y != -1){
				the_graph[x][y] = 1;
			}
		}
		x++;
	}
	put_axis(x1, x2, y1, y2);
}


void show_graph(){
	for (int y = 0; y < HEIGHT; y++){
		for(int x = 0; x < WIDTH; x++){
			if (the_graph[x][y] == 1)
				printf("*");
			if (the_graph[x][y] == 0)
				printf(" ");
			if (the_graph[x][y] == 2)
				printf("|");
			if (the_graph[x][y] == 3)
				printf("-");
		}
		printf("\n");
	}
}

int main(){
	graph(-2, WIDTH - 2, -2, HEIGHT - 2);
	printf("\n");
	show_graph();

	exit(EXIT_SUCCESS);
}
