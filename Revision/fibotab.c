#include <stdio.h>
#include <stdlib.h>

int *fibotab(int n){
	int *tab;

	tab = malloc(sizeof(int) * n);
	if (!tab)
		exit(EXIT_FAILURE);
	if (n >= 0)
		tab[0] = 1;
	if (n >= 1)
		tab[1] = 1;
	for (int count = 2; count < n; count++){
		tab[count] = tab[count - 1] + tab[count - 2];
	}
	return tab;
}

int main(){
	int *tab;
	int nb = 7;

	tab = fibotab(nb);
	for (int count = 0; count < nb; count++){
		printf("%d ", tab[count]);
	}
	free(tab);
	return 0;
}
