#include <stdio.h>

float M[3][3] = {{1.0, 2.0, 3.0}, {4.0, 5.0, 6.0}, {7.0, 8.0, 9.0}};

void totranspose(float M[3][3])
{
	printf("Matrice de départ :\n");
	for (int line = 0; line < 3; line++){
		for (int column = 0; column < 3; column++){
			printf("%.1f ", M[line][column]);
		}
		printf("\n");
	}
	printf("Matrice transposée :\n");
	for (int column = 0; column < 3; column++){
		for (int line = 0; line < 3; line++){
			printf("%.1f ", M[line][column]);
		}
		printf("\n");
	}
}


int main(){

	totranspose(M);
	return (0);
}
