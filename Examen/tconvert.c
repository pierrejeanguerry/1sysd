#include <stdio.h>
#include <stdlib.h>

float fahrenheit2celsius(float f){
	return ((f - 32.0) * 5.0 / 9.0);
}


float celsius2fahrenheit(float c){
	return ((9.0 / 5.0) * c + 32.0);
}


int main(){
	float temp;
	char choice;

	printf("Choix de la conversion ? ('1' -> Celsius to Fahrenheit; '2' -> Fahrenheit to Celsius): ");
	scanf("%c", &choice);
	printf("\nTemperature a convertir: ");
	scanf("%f", &temp);
	if (choice == '1'){
		temp = celsius2fahrenheit(temp);
	}
	else if (choice == '2'){
		temp = fahrenheit2celsius(temp);
	}
	else{
		exit(EXIT_FAILURE);
	}
	printf("Resulat de la conversion: %.2f\n", temp);
}

