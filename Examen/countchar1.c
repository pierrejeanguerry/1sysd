#include <stdio.h>
#include <stdlib.h>

int countchar1(char *str, char c){
	int count = 0;

	while (*str){
		if (*str == c){
			count++;}
		str++;
	}
	return count;
}


int main(int argc, char *argv[]){
	if (argc < 3){
		printf("veuillez entrer une chaine de caractere puis un caractere à compter dans cette chaine.\n");
	exit(EXIT_FAILURE);
	}
	printf("%d\n", countchar1(argv[1], argv[2][0]));
}
