#include <stdio.h>

int count_int(int *tab, int tab_len, int val){
	int count = 0;

	for (int cursor = 0; cursor < tab_len; cursor++){
		if (tab[cursor] == val)
			count++;
	}
	return count;
}

int main() {
    int t1[5] = { 2, 42, 1, 42, 9 };
    int t2[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };


    printf("%d dans t1 : %d fois.\n", 42, count_int(t1, 5, 42));
    printf("%d dans t1 : %d fois.\n",  2, count_int(t1, 5,  2));
    printf("%d dans t2 : %d fois.\n", 10, count_int(t2, 10, 10));
    printf("%d dans t2 : %d fois.\n", 42, count_int(t2, 10, 42));
}

