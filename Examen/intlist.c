#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>

typedef struct node node;
struct node {
   int val;
   node *next;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    return p;
}

void print_list_slow(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juset walk 
        printf("%d ", walk->val);
        fflush(stdout);
        walk = walk->next;
        sleep(1);
    }
    printf("\n");
}

node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);

    if (head == NULL) {
        head = newnode;
    } else {
        walk = head;
        while (walk->next != NULL) {
            walk = walk->next;
        }
        walk->next = newnode;
    }
    return head;
}

int detect_list_loop(node *head){
	node *turtoise, *hare;

	turtoise = head;
	hare = head;
	while (turtoise && hare)
	{
		turtoise = turtoise->next;
		hare = hare->next->next;
		if (turtoise == hare)
			return 1;
	}
	return 0;
}


// le programme affiche le contenu de la liste chainée ayant pour tête "head", mais cette liste boucle sur elle-meme
int main() {
    node *head = NULL;

    head = append_val(head, 42);
    head = append_val(head, 12);
    head = append_val(head, -5);
    head = append_val(head, 41);
    print_list_slow(head);

	// le probleme vient de cette ligne ci-dessous qui fait pointer le dernier maillon sur le deuxieme maillon
   	head->next->next->next->next = head->next;

	
	// la manière de detecter ce genre de problème serait d'utiliser le l'agorithme du lievre et de la tortue:
	// parcourir la liste de deux manière differente, si on tombe deux fois sur le même maillon,
	// c'est qu'on est dans une boucle
	if (detect_list_loop(head) == 1){
		printf("il y a une boucle\n");
		exit(EXIT_FAILURE);
	}
	
	print_list_slow(head);
}
