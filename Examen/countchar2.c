#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int countchar1(char *str, char c){
	int count = 0;

	while (*str){
		if (*str == c){
			count++;}
		str++;
	}
	return count;
}

int countchar2(char *str, char c){
	int count = 0;
	
	c = tolower(c);
	while (*str){
		*str = tolower(*str);
		if (*str == c){
			count++;}
		str++;
	}
	return count;
}


int main(int argc, char *argv[]){
	if (argc < 3){
		printf("veuillez entrer une chaine de caractere puis un caractere à compter dans cette chaine.\n");
	exit(EXIT_FAILURE);
	}
	if (argc == 4 && argv[3][0] == '-' && argv[3][1] == 'i'){
		printf("%d\n", countchar2(argv[1], argv[2][0]));
	}
	else
		printf("%d\n", countchar1(argv[1], argv[2][0]));
}

